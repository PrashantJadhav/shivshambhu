package com.pjadhav.shivshambhu;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;

import com.pjadhav.shivshambhu.adapter.ListMusicAdapter;
import com.pjadhav.shivshambhu.database.DatabaseHelper;
import com.pjadhav.shivshambhu.model.MusicFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

public class ClipListActivity extends AppCompatActivity {

    private static final String TAG = ClipListActivity.class.getName();
    private ListView lvMusicFiles;
    private ListMusicAdapter adapter;
    private List<MusicFile> mListMusicFiles;
    private DatabaseHelper mDBHelper;
    private Context mContext;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clip_list);
        lvMusicFiles = (ListView) findViewById(R.id.listview_clips);
        mDBHelper = new DatabaseHelper(this);
        mContext = ClipListActivity.this;

        //Check if db exists
        File databaseFile = getApplicationContext().getDatabasePath(DatabaseHelper.DBNAME);

        if (false == databaseFile.exists()){

            mDBHelper.getReadableDatabase();
            CopyDB(DatabaseHelper.DBNAME);
        }

        mListMusicFiles = mDBHelper.getMusicFiles();
        adapter = new ListMusicAdapter(this, mListMusicFiles);
        lvMusicFiles.setAdapter(adapter);
    }



    /*****  Function by Prashant , giving Error for Create Table **/
    /*private boolean copyDatabase (Context context) {
        try {
            InputStream inputStream = context.getAssets().open(DatabaseHelper.DBNAME);
            String outFileName = DatabaseHelper.DB_LOCATION + DatabaseHelper.DBNAME;
            OutputStream outputStream = new FileOutputStream(outFileName);
            byte[] buff = new byte[1024];
            int length = 0;

            while ((length = inputStream.read(buff))>0){
                outputStream.write(buff,0,length);
            }
            outputStream.flush();
            outputStream.close();
            Log.v("ClipListActivity", "db copied");
            return true;

        }catch (Exception ex){
            ex.printStackTrace();
            return false;
        }
    }*/



    /* New Function by Mahesh , working fine    */
    private void CopyDB(String dbname) {
        InputStream myInput;
        try {
            myInput = mContext.getAssets().open(dbname);

            // Path to the just created empty db
            String outFileName = mContext.getFilesDir().getAbsolutePath();
            outFileName = outFileName
                    .substring(0, outFileName.lastIndexOf("/"))
                    + "/databases/"
                    + dbname;

            Log.d(TAG,"outFileName : " + outFileName);

            // Open the empty db as the output stream
            OutputStream myOutput = new FileOutputStream(outFileName);
            // transfer bytes from the inputfile to the outputfile
            byte[] buffer = new byte[1024];
            int length;
            while ((length = myInput.read(buffer)) > 0) {
                myOutput.write(buffer, 0, length);
            }
            // Close the streams
            myOutput.flush();
            myOutput.close();
            myInput.close();


            Log.d(TAG,"CopyDB is Done");

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
