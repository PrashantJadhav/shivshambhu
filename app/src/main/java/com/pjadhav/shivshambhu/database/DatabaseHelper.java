package com.pjadhav.shivshambhu.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.pjadhav.shivshambhu.model.MusicFile;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pjadhav on 6/12/17.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    public static final String DBNAME = "shivaji_clips.db";
    public static final String DB_LOCATION= "/data/data/com.pjadhav.shivshambhu/databases";
    public static final String TABLE_NAME = "Clip_details";
    private Context mContext;
    private SQLiteDatabase mDatabase;

    public DatabaseHelper(Context context){
        super(context, DBNAME, null, 1);
        this.mContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public void openDatabase (){
        String dbPath= mContext.getDatabasePath(DBNAME).getPath();
        if(mDatabase != null && mDatabase.isOpen()){
            return;
        }
        mDatabase = SQLiteDatabase.openDatabase(dbPath, null, SQLiteDatabase.OPEN_READWRITE);
    }

    public void closeDatabase (){
        if(mDatabase !=null){
            mDatabase.close();
        }
    }

    public List<MusicFile> getMusicFiles () {
        MusicFile musicFile= null;
        List<MusicFile> listMusicFiles = new ArrayList<>();
        openDatabase();

        Cursor cursor = mDatabase.rawQuery("SELECT * FROM Clip_details", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()){

            musicFile = new MusicFile(cursor.getInt(0), cursor.getString(1),cursor.getString(2), cursor.getInt(3));
            listMusicFiles.add(musicFile);
            cursor.moveToNext();
        }
        cursor.close();
        closeDatabase();
        return listMusicFiles;
    }

    public int isDownloaded (String fileName){
        int isDwd = 0;
        MusicFile musicFile= null;
        openDatabase();
        Cursor cursor = mDatabase.rawQuery("Select * from Clip_details where FILE_NAME = " + "'"+fileName+"'"  , null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()){

            musicFile = new MusicFile(cursor.getInt(0), cursor.getString(1),cursor.getString(2), cursor.getInt(3));
            cursor.moveToNext();
        }
        cursor.close();
        closeDatabase();
            if(musicFile.isDownloaded() == 1){
                return 1;
            }else {
                return 0;
            }
    }

    public void setDownloaded(String fileName) {
        openDatabase();
        SQLiteDatabase database = getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("IS_DOWNLOADED", "1");

        String selection = "FILE_NAME='" + fileName + "'";

        long insertUpdateCount = 0;
        database.update(TABLE_NAME, values, selection, null);

        database.close();
       // return insertUpdateCount;
    }

}
