package com.pjadhav.shivshambhu.adapter;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.pjadhav.shivshambhu.R;
import com.pjadhav.shivshambhu.database.DatabaseHelper;
import com.pjadhav.shivshambhu.model.MusicFile;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

/**
 * Created by pjadhav on 6/12/17.
 */

public class ListMusicAdapter extends BaseAdapter {

    private Context mContext;
    private List<MusicFile> mMusicFileList;
    private DatabaseHelper mDBHelper;
    private android.os.Handler myHandler = new android.os.Handler();

    public ListMusicAdapter(Context mContext, List<MusicFile> mMusicFileList) {
        this.mContext = mContext;
        this.mMusicFileList = mMusicFileList;
        mDBHelper = new DatabaseHelper(mContext);
    }

    @Override
    public int getCount() {
        return mMusicFileList.size();
    }

    @Override
    public Object getItem(int position) {
        return mMusicFileList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return mMusicFileList.get(position).getId();
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {

        View v = View.inflate(mContext, R.layout.item_list_clip,null);

        final ImageView imgShivajiIcon = (ImageView)v.findViewById(R.id.img_shivaji);
        final ImageButton imgButtonDownload = (ImageButton)v.findViewById(R.id.img_btn_download);
        final TextView txtFileName = (TextView)v.findViewById(R.id.file_name);
        final TextView txtFilePath = (TextView)v.findViewById(R.id.file_path);

       final String fileName = mMusicFileList.get(position).getFileName();
        final String fileUrl = mMusicFileList.get(position).getFileUrl();

        txtFileName.setText(fileName);
        txtFilePath.setText(fileUrl);
        mDBHelper = new DatabaseHelper(mContext);

        if(mDBHelper.isDownloaded(fileName) == 1){
            imgButtonDownload.setBackgroundResource(R.drawable.play);
        }else {
            imgButtonDownload.setBackgroundResource(R.drawable.download);
        }

        imgButtonDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(mDBHelper.isDownloaded(fileName) == 1){
                    Toast.makeText(mContext,"Playing audio",Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(mContext,"downloaded started",Toast.LENGTH_SHORT).show();
                    new DownloadFileFromURL(fileName,fileUrl).execute();
                   // imgButtonDownload.setBackgroundResource(R.drawable.play);

                }
            }
        });

        //imgButtonDownload.setImageDrawable(Resources.getSystem().getDrawable(R.drawable.download_Icon,null));
        return v;
    }


    /**
     * Background Async Task to download file
     * */
    class DownloadFileFromURL extends AsyncTask<String, String, String> {
        String file_name, file_url;

        public DownloadFileFromURL (String fileName, String fileUrl){
            this.file_name = fileName;
            this.file_url = fileUrl;

        }

        /**
         * Before starting background thread
         * Show Progress Bar Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
           // showDialog(progress_bar_type);
        }

    /**
     * Downloading file in background thread
     * */
    @Override
    protected String doInBackground(String... f_url) {
        int count;
        try {
            URL url = new URL(file_url);
            URLConnection connection = url.openConnection();
            connection.connect();
            // this will be useful so that you can show a tipical 0-100% progress bar
            int lenghtOfFile = connection.getContentLength();

            // download the file
            InputStream input = new BufferedInputStream(url.openStream(), 8192);

            // Output stream
            OutputStream output = new FileOutputStream("/sdcard/"+file_name);

            byte data[] = new byte[1024];

            long total = 0;

            while ((count = input.read(data)) != -1) {
                total += count;
                // publishing the progress....
                // After this onProgressUpdate will be called
                publishProgress(""+(int)((total*100)/lenghtOfFile));

                // writing data to file
                output.write(data, 0, count);
            }

            // flushing output
            output.flush();

            // closing streams
            output.close();
            input.close();

        } catch (Exception e) {
            Log.e("Error: ", e.getMessage());
        }

        return null;
    }

    /**
     * Updating progress bar
     * */
    protected void onProgressUpdate(String... progress) {
        // setting progress percentage
       // pDialog.setProgress(Integer.parseInt(progress[0]));
    }

    /**
     * After completing background task
     * Dismiss the progress dialog
     * **/
    @Override
    protected void onPostExecute(String file_url) {
        // dismiss the dialog after the file was downloaded
       // dismissDialog(progress_bar_type);
        Toast.makeText(mContext, "Download completed!", Toast.LENGTH_SHORT).show();
        mDBHelper.setDownloaded(file_name);

        // btnDownload.setText("PLAY");
        //txtFileName.setText("Downloaded mp3 file");

        // Displaying downloaded image into image view
        // Reading image path from sdcard
        // String imagePath = Environment.getExternalStorageDirectory().toString() + "/downloadedfile.jpg";
        // setting downloaded into image view
        //  my_image.setImageDrawable(Drawable.createFromPath(imagePath));
    }

}

}
